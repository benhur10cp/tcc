#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <algorithm>
#include <cstring>

using namespace std;

int main(int argc, char *argv[]){
  FILE *f;
  f = fopen(argv[1], "r");

  if(f == NULL){
    fclose(f);
    return 0;
  }

  int i = 0;
  float num = 0.0;
  float ori = 0.0;
  char str[80];
  char bb[80];

  while(i <= 49){
    i ++;
    fscanf(f, "%f", &num);
    fscanf(f, "%s", str);
    if(num > ori){
      ori = num;
      int size = strlen(str);
      for(int j = 0; j < size; j ++){
        bb[j] = str[j];
      }
    }
  }

  printf("%f - %s\n", ori, bb);

  fclose(f);

  return 0;
}
