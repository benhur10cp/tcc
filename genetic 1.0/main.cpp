#include <iostream>
#include <cstring>
#include "bibs.hpp"
#include <sys/time.h>
#include "CPUTimer.cpp"
#include <stdlib.h>
#include <stdio.h>

using namespace std;

int main(int argc, char *argv[]){
	Input i;
	Printer p;
	int ar1 = atoi(argv[1]);
	int ar2 = atoi(argv[2]);
	int ar3 = atoi(argv[3]);
	double agd3 = double(ar3/10.0);

	Genetic g(ar1, ar2, agd3);/*epocas, taxa de mutação, taxa de filhos 0-1*///melhor testado mutação = 79, taxa de filhos = 0.2
	CPUTimer timer;

	i.scan_file();
	g.get_input(i);
	timer.start();
	g.algorithm_genetic();
	timer.stop();
	p.print(g, timer.getCPUTotalSecs());
	return 0;
}
