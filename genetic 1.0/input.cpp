#include "bibs.hpp"
#include <iostream>
#include <cstring>

using namespace std;

Input::Input(){
	int dimension; /*dimensoes da mochila*/
	int qtd_prod;/*quantidade de objetos*/
	int optimo;/*soluçao otima*/
	vector< vector<int> > product_w;/*peso de cada produto em cada dimensao*/
	vector<int> product_v;/*valor de cada produto*/
	vector<int> knapsak_w;/*peso suportado da mochila em cada dimensao*/
}

void Input::scan_file(){
	int x;
	cin >> dimension >> qtd_prod;


	for(int i = 0; i < qtd_prod; i ++){
		cin >> x;
		product_v.push_back(x);
	}
	
	for(int i = 0; i < dimension; i ++){
		cin >> x;
		knapsak_w.push_back(x);
	}

	for(int i = 0; i < dimension; i ++){
		vector<int> k;
		for(int j = 0; j < qtd_prod; j ++){			
			cin >> x;			
			k.push_back(x);			
		}
		product_w.push_back(k);
	}
	cin >> optimo;
}
