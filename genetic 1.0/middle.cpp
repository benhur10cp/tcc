#include <iostream>
#include <string>
#include <stdlib.h>

using namespace std;

int main(){
	double times, entry2, best_time, worse_time;
	int solves, bigger, entry1, best_solve, worse_solve;

	bigger = 48;
	times = 0.0f;
	solves = 0;
	string entry3;
	best_time = 1000000.0f;
	worse_time = -1.0f;
	best_solve = -1;
	worse_solve = 10000000;

	for(int i = 0; i < bigger; i ++){		
		getline(cin, entry3);
		entry1 = atof(entry3.c_str());
		if(entry1 < worse_solve){
			worse_solve = entry1;
		}
		if(entry1 > best_solve){
			best_solve = entry1;
		}
		solves += entry1;
		getline(cin, entry3);
		entry2 = atof(entry3.c_str());
		if(entry2 > worse_time){
			worse_time = entry2;
		}
		if(entry2 < best_time){
			best_time = entry2;
		}
		times += entry2;
	}
	cout << "Middle solves: " << solves / 48.0f << "\n";
	cout << "Middle times: " << times / 48.0f << "\n";
	cout << "Best time: " << best_time << "\n";
	cout << "Worse time: " << worse_time << "\n";
	cout << "Best solve: " << best_solve << "\n";
	cout << "Worse solve: " << worse_solve << "\n";

}