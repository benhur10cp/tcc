#include <vector>


//----------------------------------------------------------------------------//
//input//

#ifndef INPUT_H
#define INPUT_H

#include <iostream>
#include <cstring>

using namespace std;

class Input {
private:

public:
	Input();
	void scan_file();
	int dimension;
	int qtd_prod;
	int optimo;
	vector < vector <int> > product_w;
	vector <int> product_v;
	vector <int> knapsak_w;
};

#endif

//----------------------------------------------------------------------------//
//heuristc genetic//

#ifndef h_GENETIC_H
#define h_GENETIC_H

#include <iostream>
#include <cstring>

using namespace std;

class Genetic {

private:
	int dimension;
	int qtd_prod;

	vector < vector <int> > product_w;
	vector <int> product_v;
	vector <int> knapsak_w;
	int period;
	vector <bool> chromosome;
	int ratio_mutation;
	double child_create;
	vector <double> fitness;

public:
	Genetic(int p, int rm, double cc);
	void get_input(Input in);
	void algorithm_genetic();
	int new_value;
	int optimo;
};

#endif

//----------------------------------------------------------------------------//
//printer//

#ifndef PRINTER_H
#define PRINTER_H

#include <iostream>
#include <cstring>

using namespace std;

class Printer {
private:
	int solve;

public:
	Printer();
	void print(Genetic c, double times);
};

#endif
