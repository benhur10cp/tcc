#include "bibs.hpp"
#include <iostream>
#include <cstring>
#include <time.h>
#include <stdlib.h>
#include <algorithm>
#include <utility>
#include <map>
#include <stdio.h>

using namespace std;

struct cmp{//comparador do pair ofit
  bool operator () (const pair<int,double> &a , const pair<int,double> &b){
    return (b.second > a.second);
  }
}cmp;

Genetic::Genetic(int p, int rm, double cc){
	int dimension;
	int qtd_prod;

	vector < vector <int> > product_w;
	vector <int> product_v;
	vector <int> knapsak_w;
	vector <double> fitness;

	int optimo;
	int new_value; /*valor da solução final*/
	int period; /*por quantos ciclos de evolução o algoritmo vai executar*/
	this-> period = p;
	vector<bool> chromosome;/*vetor que guarda se o objeto foi ou não selecionado para a solução*/
	int ratio_mutation;/*taxa de mutação do algoritmo, 0 a 1000*/
	double child_create;
	this-> ratio_mutation = rm;
	this-> child_create = cc;
}

void Genetic::get_input(Input in){//recebendo os dados da entrada lida
	this->dimension = in.dimension;
	this->qtd_prod = in.qtd_prod;
	this->optimo = in.optimo;
	this->product_w = in.product_w;
	this->product_v = in.product_v;
	this->knapsak_w = in.knapsak_w;
}

void Genetic::algorithm_genetic(){

	bool flag;
	int rc;//usado como indice do produto escolhido
	double r = 0.0;//auxilia no calculo da função fitness
	double sum = 0.0;//soma todos os valores fitness
	int max_c = 0;//soma todos os valores dos produtos
	vector<int> weight;//peso atual da mochila

	int n_child/*quantidade de filhos criados*/, random, max_randomic;
	double ad;

	//int period_rest;

	vector <int> child;
	vector <int> child2;
	vector <bool> ccrom;

	vector <pair <int,double> > ofit;//guarda os fitness de forma ordenada

	int dec = 0;
  int value;
  int pos;

	for(int i = 0; i < qtd_prod; i ++){
		chromosome.push_back(false);
		fitness.push_back(0.0);
		child.push_back(0);
		child2.push_back(0);
		ccrom.push_back(false);
	}

	for(int i = 0; i < dimension; i ++){
		weight.push_back(0);
	}

	srand (time(0));

	/*inicializando a população inicial*/
	for(int i = 0; i < qtd_prod; i ++){
		flag = false;
		rc = rand() % (qtd_prod - 1);
		for(int j = 0; j < dimension; j ++){
			if((product_w[j][rc] + weight[j]) > knapsak_w[j]){
				flag = true;
				break;
			}
		}

		if(!flag){
			for(int j = 0; j < dimension; j ++){
				weight[j] += product_w[j][rc];
			}
			chromosome[rc] = true;
		}
	}

	/*calcula aptidão de cada produto*/
	/*somatoria dos pesos do produto i, dividido pelo valor do produto i. quanto menor o valor do fitness pior o produto é*/
	for(int i = 0; i < qtd_prod; i ++){
		for(int j = 0; j < dimension; j ++){
			if(product_v[i] == 0){
				r += product_w[j][i];
			}else{
				r += double((double)product_w[j][i]);
			}
		}
		int emp = 0;
		r = double((double)product_v[i] / (double)r);

		fitness[i] = r;
		sum += fitness[i];
		r = 0.0;
		max_c += product_v[i];
		ofit.push_back(make_pair(i, fitness[i]));
	}

	r = sum / qtd_prod;

	sort(ofit.begin(), ofit.end(), cmp);

	for(int i = 0; i < dimension; i ++){
		weight[i] = 0;
	}

	new_value = 0;

	/*Laço de periodo*/
	while(period){

		/*seleciona pais*/
		map <int, double> fit;
		map <int, double> :: iterator it;

		ad = 0.0;

		for(int i = 0; i < qtd_prod; i ++){
			ad += ofit[i].second;
			fit[i] = ad;
			child[i] = 0;
			child2[i] = 0;
			ccrom[i] = false;
		}
		n_child = child_create * qtd_prod;//numeros de filhos a serem gerados
		max_randomic = ad + 1;

		if((n_child % 2) != 0){
			n_child ++;
		}

		int cont = 0;

		while((n_child / 2) != cont){//gerando os filhos
			random = rand() % max_randomic;//escolhendo um valor fitness
			for(it = fit.end(); it != fit.begin(); it --){
				if((it->second >= random) && (it->second != -1)){//se o filho for na faixa e se não tiver sido escolhido
					child[it->first] = 1;
					it->second = -1;
					cont ++;
					break;
				}
			}
		}

		cont = 0;

    while((n_child / 2) != cont){//gerando os filhos
			random = rand() % max_randomic;//escolhendo um valor fitness
			for(it = fit.end(); it != fit.begin(); it --){
				if((it->second >= random) && (it->second != -1)){//se o filho for na faixa e se não tiver sido escolhido
					child2[it->first] = 1;
					it->second = -1;
					cont ++;
					break;
				}
			}
		}

		/*cruzamento*/
		int cr1 = rand() % int(0.40 * qtd_prod) + int(0.10 * qtd_prod);
		int cr2 = rand() % int(0.90 * qtd_prod) + int(0.60 * qtd_prod);
    //cruzamento em duas regiões
		for(int i = 0; i < qtd_prod; i ++){
			if(i <= cr1){
				child[i] = child2[i];
			}else if(i > cr1 && i <= cr2){
				child2[i] = child[i];
			}else{
				child[i] = child2[i];
			}
		}

		/*mutação*/
		int m;
		for(int i = 0; i < qtd_prod; i ++){
			m = rand() % 100;
			if(m <= ratio_mutation){
        child[i] = !child[i];
			}
			m = rand() % 100;
			if(m <= ratio_mutation){
        child2[i] = !child2[i];
			}
		}

		/*avaliação dos filhos*/

    int rr;
    for(int i = 0; i < qtd_prod; i ++){
        rr = rand() % 1;
        if(rr == 1){
          if(child[i] == 1){
            chromosome[i] = true;
          }else if(child[i] == 0){
            chromosome[i] = false;
          }
        }else if(rr == 0){
          if(child2[i] == 1){
            chromosome[i] = true;
          }else if(child2[i] == 0){
            chromosome[i] = false;
          }
        }
    }

		/*calcula a viabilidade da solução*/
		for(int i = 0; i < dimension; i ++){
			weight[i] = 0;
		}

		flag = false;
		for(int i = 0; i < dimension; i ++){
			for(int j = 0; j < qtd_prod; j ++){
				if(chromosome[j] == 1){
					weight[i] += product_w[i][j];
				}
			}
			if(weight[i] > knapsak_w[i]){
				flag = true;//tem que fazer a reparação
			}
		}

		/*Reparar solução*/
		if(flag){
			//FASE DROP
			for(int i = 0; i < qtd_prod; i ++){
				flag = false;
				pos = ofit[i].first;

				if(chromosome[pos] == 1){
					chromosome[pos] = 0;

					for(int j = 0; j < dimension; j ++){
						weight[j] -= product_w[j][pos];
						if(weight[j] > knapsak_w[j]){
							flag = true;
						}
					}
					if(!flag){
						break;
					}
				}
			}

			flag = false;
			//FASE ADD
			for(int i = qtd_prod - 1; i >= 0; i --){
				pos = ofit[i].first;
				flag = false;

				if(chromosome[pos] == 0){
					for(int j = 0; j < dimension; j ++){
						if((weight[j] + product_w[j][pos]) <= knapsak_w[j]){
							flag = true;
						}else{
							flag = false;
							break;
						}
					}
					if(flag){
						for(int j = 0; j < dimension; j ++){
							weight[j] += product_w[j][pos];
						}
						chromosome[pos] = 1;
					}
				}
			}
		}//fim da fase de reparação

    value = 0;
		for(int i = 0; i < qtd_prod; i ++){//calcula o valor da nova solução
			if(chromosome[i] == 1){
				value += product_v[i];
			}
		}

		if(value > new_value){//verifica se a nova solução é a melhor
			new_value = value;
		}

		period --;
		dec += value - new_value;
		if(((dec * -1) > (optimo * 10)) || (new_value == optimo)){
			break;
		}
	}
}
