#include <iostream>
#include <cstring>
#include "bibs.hpp"
#include <sys/time.h>
#include "CPUTimer.cpp"
#include <stdlib.h>
#include <stdio.h>

using namespace std;

int main(int argc, char *argv[]){
	Input i;
	Printer p;
	CPUTimer timer;

	int ar1 = atoi(argv[1]);

	if(ar1 == 1){
		cout << "|GENÉTICO|\n";
	}else if(ar1 == 2){
		//cout << "|TABU|\n";

		int ar2 = atoi(argv[2]);
		int ar3 = atoi(argv[3]);

		Tabu t(ar2, ar3);

		i.scan_file();
		t.get_input(i);
		timer.start();
		t.tabu_search();
		timer.stop();
		p.print(t, timer.getCPUTotalSecs());
		return 0;

	}else{
		return 0;
	}
}
