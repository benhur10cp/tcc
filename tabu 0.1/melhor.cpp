#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <algorithm>
#include <cstring>

using namespace std;

int main(int argc, char *argv[]){
  FILE *f;
  f = fopen(argv[1], "r");

  if(f == NULL){
    fclose(f);
    return 0;
  }

  int i = 0;
  float sum, num;
  sum = num = 0.0;

  while(i <= 49){
    i ++;
    fscanf(f, "%f", &num);
    sum += num;
  }
  printf("%f\n%s\n", float((float)sum/49.0), argv[1]);

  fclose(f);

  return 0;
}
