#include "bibs.hpp"
#include <iostream>
#include <cstring>
#include <time.h>
#include <stdlib.h>
#include <algorithm>
#include <utility>
#include <stdio.h>
#include <vector>

using namespace std;

struct cmpDec{//comparador do pair ofit, ordena decrescente
  bool operator () (const pair<int,double> &a , const pair<int,double> &b){
    return (b.second < a.second);
  }
}cmpDec;


struct cmpCre{//comparador do pair ofit, ordena crescente
  bool operator () (const pair<int,double> &a , const pair<int,double> &b){
    return (b.second > a.second);
  }
}cmpCre;


Tabu::Tabu(int size, int t){

	int dimension;
	int qtd_prod;

	vector < vector <int> > product_w;
	vector <int> product_v;
	vector <int> knapsak_w;

	int optimo;
	int new_value; /*valor da solução final*/
  int tabu_size;/*tamanho da lista tabu*/

  int times;
  int size_tb;
  this->times = t;
  this->size_tb = size;

  vector <pair <int,double> > u_cresc;
  vector <pair <int,double> > u_decr;

  vector <pair <int,int> > frequency_add;//produtos que mais mudam na solução, mais entram
  vector <pair <int,int> > frequency_drop;//produtos que mais mudam na solução, mais saem
  vector <pair <int,int> > recency;//produtor que mais ficam na solução


  vector <int> sum_prod;
  vector <int> x_solve;
	vector <int> tabu_list;
	vector <int> weight_res;
}

void Tabu::get_input(Input in){
	this->dimension = in.dimension;
	this->qtd_prod = in.qtd_prod;
	this->optimo = in.optimo;
	this->product_w = in.product_w;
	this->product_v = in.product_v;
	this->knapsak_w = in.knapsak_w;
}


void Tabu::tabu_search(){
  bool stop;
  int direction;

  stop = false;
  new_value = -1;

  for(int i = 0; i < qtd_prod; i ++){
    x_solve.push_back(0);
    sum_prod.push_back(0);
    recency.push_back(make_pair(i, 0));
    frequency_drop.push_back(make_pair(i, 0));
    frequency_add.push_back(make_pair(i, 0));
  }

  for(int i = 0; i < dimension; i ++){
    weight_res.push_back(knapsak_w[i]);
  }

  for(int i = 0; i < qtd_prod; i ++){
    for(int j = 0; j < dimension; j ++){
      sum_prod[i] += product_w[j][i];
    }
  }

  for(int i = 0; i < qtd_prod; i ++){
   u_cresc.push_back(make_pair(i ,((double)product_v[i] / double(product_v[i] * sum_prod[i]))));
  }

  u_decr = u_cresc;

  sort(u_cresc.begin(), u_cresc.end(), cmpCre);
  sort(u_decr.begin(), u_decr.end(), cmpDec);

  tabu_size = int(qtd_prod * double(size_tb/10.0));
  direction = 0;
  int it = 0;
  int b;

  while(!stop){
    if(direction == 0){
      ts_add();//guloso para entrada
      b = calculate();
      if(b == 1){
        it = 0;
      }else{
        it ++;
      }
      ts_complement();//intensificação
      b = calculate();
      if(b == 1){
        it = 0;
      }else{
        it ++;
      }
      ts_drop();//diversificação
      b = calculate();
      if(b == 1){
        it = 0;
      }else{
        it ++;
      }
      memory();
      direction = 1;
    }else{
    //----------------------
      ts_project();//guloso para saida
      b = calculate();
      if(b == 1){
        it = 0;
      }else{
        it ++;
      }
      ts_complement();//intensificação
      b = calculate();
      if(b == 1){
        it = 0;
      }else{
        it ++;
      }
      ts_infeasible();//diversificação
      b = calculate();
      if(b == 1){
        it = 0;
      }else{
        it ++;
      }
      memory();
      direction = 0;
    }
    if(new_value == optimo || it >= qtd_prod * times)
      break;
  }
}

void Tabu::ADD(int i){
  x_solve[i] = 1;
  for(int j = 0; j < dimension; j ++){
    weight_res[j] -= product_w[j][i];
  }
  for(int k = 0; k < qtd_prod; k ++){
    if(i == frequency_add[k].first){
      frequency_add[k].second ++;
      break;
    }
  }
  sort(frequency_add.begin(), frequency_add.end(), cmpCre);
}

void Tabu::DROP(int i){
  x_solve[i] = 0;
  for(int j = 0; j < dimension; j ++){
    weight_res[j] += product_w[j][i];
  }
  for(int k = 0; k < qtd_prod; k ++){
    if(i == frequency_drop[k].first){
      frequency_drop[k].second ++;
      break;
    }
  }
  sort(frequency_drop.begin(), frequency_drop.end(), cmpCre);
}

void Tabu::memory(){
  for(int i = 0; i < qtd_prod; i ++){
    if(x_solve[i] == 1){
      for(int k = 0; k < qtd_prod; k ++){
        if(i == recency[k].first){
          recency[k].second ++;
          break;
        }
      }
    }
  }
  sort(recency.begin(), recency.end(), cmpCre);
}

int Tabu::calculate(){

  int sum;
  sum = 0;
  for(int i = 0; i < qtd_prod; i ++){
    sum += x_solve[i] * product_v[i];
  }
  if(sum > new_value){
    new_value = sum;
    return 1;
  }else{
    return 0;
  }
}

void Tabu::ts_add(){
  int flag, flag2;

  for(int i = 0; i < qtd_prod; i ++){
    for(int k = 0; k < dimension; k ++){
      if(weight_res[k] - product_w[k][u_decr[i].first] < 0){
        flag = 1;
        break;
      }
    }
    flag2 = 0;
    for(int j = 0; j < tabu_list.size(); j ++){
      if(u_decr[i].first == tabu_list[j]){
        flag2 = 1;
        break;
      }
    }
    if(x_solve[u_decr[i].first] == 0 && flag == 0 && flag2 == 0){
      ADD(u_decr[i].first);
      if(tabu_list.size() == tabu_size)
        tabu_list.erase(tabu_list.begin());
      tabu_list.push_back(u_decr[i].first);
    }else if(x_solve[u_decr[i].first] == 0 && flag == 0 && flag2 == 1){//criterio de aspiração
      int sum;
      sum = 0;
      for(int j = 0; j < qtd_prod; j ++){
        if(j == u_decr[i].first)
          sum += 1 * product_v[j];
        else
          sum += x_solve[j] * product_v[j];
      }
      if(sum > new_value){
        ADD(u_decr[i].first);
        if(tabu_list.size() == tabu_size)
          tabu_list.erase(tabu_list.begin());
        tabu_list.push_back(u_decr[i].first);
      }
    }
  }
}

void Tabu::ts_project(){
  int flag, flag2;

  for(int i = 0; i < qtd_prod; i ++){
    flag2 = 0;
    for(int j = 0; j < tabu_list.size(); j ++){
      if(u_cresc[i].first == tabu_list[j]){
        flag2 = 1;
        break;
      }
    }
    if(x_solve[u_cresc[i].first] == 1 && flag2 == 0){
      DROP(u_cresc[i].first);
      if(tabu_list.size() == tabu_size)
       tabu_list.erase(tabu_list.begin());
      tabu_list.push_back(u_cresc[i].first);
    }
  }

}

void Tabu::ts_complement(){
  int flag, flag2;
  int sum;
  flag = 0;
  flag2 = 0;
  sum = 0;

  int f = qtd_prod - 1;

  for(int i = 0; i < qtd_prod; i ++){
    if(x_solve[frequency_add[f].first] == 0){
      //------------------------------------------------------------------------------------------------------
      for(int k = 0; k < dimension; k ++){
        if(weight_res[k] - product_w[k][frequency_add[f].first] < 0){
          flag = 1;
          break;
        }
      }
      flag2 = 0;
      for(int j = 0; j < tabu_list.size(); j ++){
        if(frequency_add[f].first == tabu_list[j]){
          flag2 = 1;
          break;
        }
      }

      if(x_solve[frequency_add[f].first] == 0 && flag == 0 && flag2 == 0){
        ADD(frequency_add[f].first);
        if(tabu_list.size() == tabu_size)
          tabu_list.erase(tabu_list.begin());
        tabu_list.push_back(frequency_add[f].first);
      }else if(x_solve[frequency_add[f].first] == 0 && flag == 0 && flag2 == 1){//criterio de aspiração
        int sum;
        sum = 0;
        for(int j = 0; j < qtd_prod; j ++){
          if(j == frequency_add[f].first)
            sum += 1 * product_v[j];
          else
            sum += x_solve[j] * product_v[j];
        }
        if(sum > new_value){
          ADD(frequency_add[f].first);
          if(tabu_list.size() == tabu_size)
            tabu_list.erase(tabu_list.begin());
          tabu_list.push_back(frequency_add[f].first);
        }
      }
      //------------------------------------------------------------------------------------------------------
    }
    if(x_solve[frequency_drop[f].first] == 1){
      //------------------------------------------------------------------------------------------------
      flag2 = 0;
      for(int j = 0; j < tabu_list.size(); j ++){
        if(frequency_drop[f].first == tabu_list[j]){
          flag2 = 1;
          break;
        }
      }
      if(x_solve[frequency_drop[f].first] == 1 && flag2 == 0){
        DROP(frequency_drop[f].first);
        if(tabu_list.size() == tabu_size)
         tabu_list.erase(tabu_list.begin());
        tabu_list.push_back(frequency_drop[f].first);
      }
      //------------------------------------------------------------------------------------------------
    }
    f --;
  }

}

void Tabu::ts_drop(){
  int f, flag, flag2;

  for(int i = 0; i < qtd_prod; i ++){
    if(x_solve[recency[i].first] == 0){
      for(int k = 0; k < dimension; k ++){
        if(weight_res[k] - product_w[k][recency[i].first] < 0){
          flag = 1;
          break;
        }
      }
      flag2 = 0;
      for(int j = 0; j < tabu_list.size(); j ++){
        if(recency[i].first == tabu_list[j]){
          flag2 = 1;
          break;
        }
      }

      if(x_solve[recency[i].first] == 0 && flag == 0 && flag2 == 0){
        ADD(recency[i].first);
        if(tabu_list.size() == tabu_size)
          tabu_list.erase(tabu_list.begin());
        tabu_list.push_back(recency[i].first);
      }else if(x_solve[recency[i].first] == 0 && flag == 0 && flag2 == 1){//criterio de aspiração
        int sum;
        sum = 0;
        for(int j = 0; j < qtd_prod; j ++){
          if(j == recency[i].first)
            sum += 1 * product_v[j];
          else
            sum += x_solve[j] * product_v[j];
        }
        if(sum > new_value){
          ADD(recency[i].first);
          if(tabu_list.size() == tabu_size)
            tabu_list.erase(tabu_list.begin());
          tabu_list.push_back(recency[i].first);
        }
      }

    }

    if(x_solve[recency[f].first] == 1){
      flag2 = 0;
      for(int j = 0; j < tabu_list.size(); j ++){
        if(recency[f].first == tabu_list[j]){
          flag2 = 1;
          break;
        }
      }
      if(x_solve[recency[f].first] == 1 && flag2 == 0){
        DROP(recency[f].first);
        if(tabu_list.size() == tabu_size)
         tabu_list.erase(tabu_list.begin());
        tabu_list.push_back(recency[f].first);
      }
    }
  }
}

void Tabu::ts_infeasible(){
  int flag, flag2;
  int f;

  f = qtd_prod - 1;

  for(int i = 0; i < qtd_prod; i ++){
    if(x_solve[frequency_add[i].first] == 0){

      for(int k = 0; k < dimension; k ++){
        if(weight_res[k] - product_w[k][frequency_add[i].first] < 0){
          flag = 1;
          break;
        }
      }
      flag2 = 0;
      for(int j = 0; j < tabu_list.size(); j ++){
        if(frequency_add[i].first == tabu_list[j]){
          flag2 = 1;
          break;
        }
      }

      if(x_solve[frequency_add[i].first] == 0 && flag == 0 && flag2 == 0){
        ADD(frequency_add[i].first);
        if(tabu_list.size() == tabu_size)
          tabu_list.erase(tabu_list.begin());
        tabu_list.push_back(frequency_add[i].first);
      }else if(x_solve[recency[i].first] == 0 && flag == 0 && flag2 == 1){//criterio de aspiração
        int sum;
        sum = 0;
        for(int j = 0; j < qtd_prod; j ++){
          if(j == frequency_add[i].first)
            sum += 1 * product_v[j];
          else
            sum += x_solve[j] * product_v[j];
        }
        if(sum > new_value){
          ADD(frequency_add[i].first);
          if(tabu_list.size() == tabu_size)
            tabu_list.erase(tabu_list.begin());
          tabu_list.push_back(frequency_add[i].first);
        }
      }

    }
    if(x_solve[frequency_drop[i].first] == 1){

      flag2 = 0;
      for(int j = 0; j < tabu_list.size(); j ++){
        if(frequency_drop[i].first == tabu_list[j]){
          flag2 = 1;
          break;
        }
      }
      if(x_solve[frequency_drop[i].first] == 1 && flag2 == 0){
        DROP(frequency_drop[i].first);
        if(tabu_list.size() == tabu_size)
         tabu_list.erase(tabu_list.begin());
        tabu_list.push_back(frequency_drop[i].first);
      }

    }
  }
}
