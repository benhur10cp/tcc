#include <vector>


//----------------------------------------------------------------------------//
//input//

#ifndef INPUT_H
#define INPUT_H

#include <iostream>
#include <cstring>

using namespace std;

class Input {
private:

public:
	Input();
	void scan_file();
	int dimension;
	int qtd_prod;
	int optimo;
	vector < vector <int> > product_w;
	vector <int> product_v;
	vector <int> knapsak_w;
};

#endif

//----------------------------------------------------------------------------//
//heuristc genetic//

#ifndef h_GENETIC_H
#define h_GENETIC_H

#include <iostream>
#include <cstring>

using namespace std;

class Genetic {

private:
	int dimension;
	int qtd_prod;

	vector < vector <int> > product_w;
	vector <int> product_v;
	vector <int> knapsak_w;
	int period;
	vector <bool> chromosome;
	int ratio_mutation;
	double child_create;
	vector <double> fitness;

public:
	Genetic(int p, int rm, double cc);
	void get_input(Input in);
	void algorithm_genetic();
	int new_value;
	int optimo;
};

#endif

//----------------------------------------------------------------------------//
//tabu//
#ifndef h_TABU_H
#define h_TABU_H

#include <iostream>
#include <cstring>

using namespace std;

class Tabu {

private:
	int dimension;
	int qtd_prod;
	int tabu_size;
	int size_tb;
	int times;

	vector <pair <int,int> > frequency_add;//produtos que mais mudam na solução, mais entram
  vector <pair <int,int> > frequency_drop;//produtos que mais mudam na solução, mais saem
	vector <pair <int,int> > recency;//produtos que mais ficam na solução

	vector <pair <int,double> > u_cresc;
	vector <pair <int,double> > u_decr;

	vector < vector <int> > product_w;
	vector <int> product_v;
	vector <int> knapsak_w;

  vector <int> sum_prod;
	vector <int> x_solve;
	vector <int> tabu_list;

	vector <int> weight_res;

	void memory();
	int calculate();

	void ADD(int i);
	void DROP(int i);
	void ts_add();
	void ts_drop();
	void ts_project();
	void ts_complement();
	void ts_infeasible();

public:
	Tabu(int size_tb, int t);
	void get_input(Input in);
	void tabu_search();
	int new_value;
	int optimo;
};

#endif

//----------------------------------------------------------------------------//
//printer//

#ifndef PRINTER_H
#define PRINTER_H

#include <iostream>
#include <cstring>

using namespace std;

class Printer {
private:
	int solve;

public:
	Printer();
	void print_genetic(Genetic c, double times);
	void print_tabu(Tabu t, double times);
};

#endif
