#include <iostream>
#include <cstring>
#include "bibs.hpp"
#include <sys/time.h>
#include "CPUTimer.cpp"
#include <stdlib.h>
#include <stdio.h>

using namespace std;

int main(int argc, char *argv[]){
	Input i;
	Printer p;
	CPUTimer timer;

	int ar1 = atoi(argv[1]);

	if(ar1 == 1){
		cout << "|GENÉTICO|\n";

		int ar2 = atoi(argv[2]);
		int ar3 = atoi(argv[3]);
		int ar4 = atoi(argv[4]);
		double agd3 = double(ar4/10.0);

		Genetic g(ar2, ar3, agd3);/*epocas, taxa de mutação, taxa de filhos 0-1*///melhor testado mutação = 79, taxa de filhos = 0.2
		CPUTimer timer;

		i.scan_file();
		g.get_input(i);
		timer.start();
		g.algorithm_genetic();
		timer.stop();
		p.print_genetic(g, timer.getCPUTotalSecs());

	}else if(ar1 == 2){
		cout << "|TABU|\n";

		int ar2 = atoi(argv[2]);
		int ar3 = atoi(argv[3]);

		Tabu t(ar2, ar3);/*tamanho lista tabu, multiplicador de iteraões maximas*/

		i.scan_file();
		t.get_input(i);
		timer.start();
		t.tabu_search();
		timer.stop();
		p.print_tabu(t, timer.getCPUTotalSecs());

	}else{
		return 0;
	}
	return 0;
}
